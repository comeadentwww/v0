﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Earth.Ear.Ot.FantasyFootball.DataTransferObjects.PremierLeague;
using Earth.Ear.Ot.FantasyFootball.External;
using Earth.Ear.Ot.FantasyFootball.Models;
using Earth.Ear.Ot.FantasyFootball.WebApi;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Rewrite.Internal.ApacheModRewrite;

namespace Earth.Ear.Ot.FantasyFootball.Controllers.web
{
    public class TeamController : Controller
    {
        [HttpGet]
        public async Task<ActionResult> TeamList()
        {
            FantasyFootballDto responseDto = null;

            using (var apiClient = new ApiClientJson("https://fantasy.premierleague.com/drf/bootstrap-static"))
            {
                var response = await apiClient.GetAsync<FantasyFootballDto>(null, null);

                // TODO: Error Handling:.

                responseDto = response.ResponseDto;
            }

            var viewModel = new TeamViewModel()
            {
                FantasyFootballDto = responseDto
            };

            return View(viewModel);
        }

        [HttpGet]
        public async Task<ActionResult> TeamDetails(int teamId)
        {
            FantasyFootballDto responseDto = null;

            using (var apiClient = new ApiClientJson("https://fantasy.premierleague.com/drf/bootstrap-static"))
            {
                var response = await apiClient.GetAsync<FantasyFootballDto>(null, null);

                // TODO: Error Handling:.

                responseDto = response.ResponseDto;
            }

            var viewModel = new TeamViewModel()
            {
                FantasyFootballDto = responseDto,
                SelectedTeamId = teamId
            };

            return View(viewModel);
        }

        [HttpGet]
        public async Task<ActionResult> ProcessTeams()
        {
            var fantasyFootballBdo = await PremierLeague.GetFantasyFootball();

            var teamsDocument = new TeamsDocument();

            var fileName = teamsDocument.Create(fantasyFootballBdo);

            var viewModel = new ProcessTeamsViewModel()
            {
                FileName = fileName
            };

            return View(viewModel);
        }

        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Team/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Team/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Team/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Team/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Team/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Team/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
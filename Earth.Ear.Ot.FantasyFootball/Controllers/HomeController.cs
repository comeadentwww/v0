﻿using Earth.Ear.Ot.FantasyFootball.DataTransferObjects.PremierLeague;
using Earth.Ear.Ot.FantasyFootball.Models;
using Earth.Ear.Ot.FantasyFootball.WebApi;
using Microsoft.AspNetCore.Mvc;
using Spire.Doc;
using Spire.Doc.Documents;
using Spire.Doc.Fields;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Threading.Tasks;

namespace Earth.Ear.Ot.FantasyFootball.Controllers
{
    public class HomeController : Controller
    {
        private static void CreateTable()
        {
            //Create Table
            Document doc = new Document();

#if true
            Section s = doc.AddSection();
            Table table = s.AddTable(true);

            //Create Header and Data
            String[] Header = { "Item", "Description", "Qty", "Unit Price", "Price" };
            String[][] data = {
                                  new String[]{ "Spire.Doc for .NET",".NET Word Component","1","$799.00","$799.00"},
                                  new String[]{"Spire.XLS for .NET",".NET Excel Component","2","$799.00","$1,598.00"},
                                  new String[]{"Spire.Office for .NET",".NET Office Component","1","$1,899.00","$1,899.00"},
                                  new String[]{"Spire.PDF for .NET",".NET PDFComponent","2","$599.00","$1,198.00"},
                              };
            //Add Cells
            table.ResetCells(data.Length + 1, Header.Length);

            //Header Row
            TableRow FRow = table.Rows[0];
            FRow.IsHeader = true;
            //Row Height
            FRow.Height = 23;
            //Header Format
            FRow.RowFormat.BackColor = Color.AliceBlue;
            for (int i = 0; i < Header.Length; i++)
            {
                //Cell Alignment
                Paragraph p = FRow.Cells[i].AddParagraph();
                FRow.Cells[i].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                p.Format.HorizontalAlignment = HorizontalAlignment.Center;
                //Data Format
                TextRange TR = p.AppendText(Header[i]);
                TR.CharacterFormat.FontName = "Calibri";
                TR.CharacterFormat.FontSize = 14;
                TR.CharacterFormat.TextColor = Color.Teal;
                TR.CharacterFormat.Bold = true;
            }

            //Data Row
            for (int r = 0; r < data.Length; r++)
            {
                TableRow DataRow = table.Rows[r + 1];

                //Row Height
                DataRow.Height = 20;

                //C Represents Column.
                for (int c = 0; c < data[r].Length; c++)
                {
                    //Cell Alignment
                    DataRow.Cells[c].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                    //Fill Data in Rows
                    Paragraph p2 = DataRow.Cells[c].AddParagraph();
                    TextRange TR2 = p2.AppendText(data[r][c]);
                    //Format Cells
                    p2.Format.HorizontalAlignment = HorizontalAlignment.Center;
                    TR2.CharacterFormat.FontName = "Calibri";
                    TR2.CharacterFormat.FontSize = 12;
                    TR2.CharacterFormat.TextColor = Color.Brown;
                }
            }
#endif

            Section teamSection = doc.AddSection();

            var teamNamePara = teamSection.AddParagraph();

            var textRange = teamNamePara.AppendText("Arsenal");
            textRange.CharacterFormat.Bold = true;
            textRange.CharacterFormat.Italic = true;
                
            Table teamTable = teamSection.AddTable(true);

            //Create Header and Data
            String[] teamHeader = { "Player", "Cost", "Points", "Injury" };

            //Add Cells
            teamTable.ResetCells(5, teamHeader.Length);

            //Header Row
            TableRow headerRow = teamTable.Rows[0];
            headerRow.IsHeader = true;
            //Row Height
            headerRow.Height = 23;
            //Header Format
            headerRow.RowFormat.BackColor = Color.AliceBlue;
            for (int i = 0; i < teamHeader.Length; i++)
            {
                //Cell Alignment
                Paragraph headerParagraph = headerRow.Cells[i].AddParagraph();
                headerRow.Cells[i].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                headerParagraph.Format.HorizontalAlignment = HorizontalAlignment.Center;
                //Data Format
                TextRange TR = headerParagraph.AppendText(teamHeader[i]);
                TR.CharacterFormat.FontName = "Calibri";
                TR.CharacterFormat.FontSize = 14;
                TR.CharacterFormat.TextColor = Color.Teal;
                TR.CharacterFormat.Bold = true;
            }

            TableRow tableRow = null;

            tableRow = teamTable.Rows[1];

            tableRow.Height = 20;

            for (int columnIndex = 0; columnIndex < 4; ++columnIndex)
            {
                tableRow.Cells[columnIndex].CellFormat.VerticalAlignment = VerticalAlignment.Middle;

                var innerPara = tableRow.Cells[columnIndex].AddParagraph();
                TextRange TR2 = innerPara.AppendText("Arsenal Goalkeepers");
                //Format Cells
                innerPara.Format.HorizontalAlignment = HorizontalAlignment.Left;
                TR2.CharacterFormat.FontName = "Calibri";
                TR2.CharacterFormat.FontSize = 12;
                TR2.CharacterFormat.Italic = true;
                TR2.CharacterFormat.TextColor = Color.Brown;
            }

            tableRow = teamTable.Rows[2];

            tableRow.Height = 20;

            for (int columnIndex = 0; columnIndex < 4; ++columnIndex)
            {
                tableRow.Cells[columnIndex].CellFormat.VerticalAlignment = VerticalAlignment.Middle;

                var innerPara = tableRow.Cells[columnIndex].AddParagraph();
                TextRange TR2 = innerPara.AppendText("Arsenal Goalkeepers");
                //Format Cells
                innerPara.Format.HorizontalAlignment = HorizontalAlignment.Left;
                TR2.CharacterFormat.FontName = "Arial";
                TR2.CharacterFormat.FontSize = 12;
                TR2.CharacterFormat.Italic = true;
                TR2.CharacterFormat.TextColor = Color.Brown;
            }

            //Save and Launch
            doc.SaveToFile("WordTable.docx", FileFormat.Docx2013);
            //System.Diagnostics.Process.Start("WordTable.docx");
        }

        public async Task<IActionResult> Index()
        {
            using (var apiClient = new ApiClientJson("https://fantasy.premierleague.com/drf/bootstrap-static"))
            {
                var response = await apiClient.GetAsync<FantasyFootballDto>(null, null);

                 
                CreateTable();

            }

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

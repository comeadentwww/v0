﻿namespace Earth.Ear.Ot.FantasyFootball.DataTransferObjects.Ajax
{
    public class ContactResponseDto
    {
        public bool Success { get; set; }

        public decimal Score { get; set; }
    }
}

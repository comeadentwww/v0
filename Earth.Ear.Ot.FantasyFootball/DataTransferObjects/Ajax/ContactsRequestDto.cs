﻿namespace Earth.Ear.Ot.FantasyFootball.DataTransferObjects.Ajax
{
    public class ContactRequestDto
    {
        public string Token { get; set; }
    }
}

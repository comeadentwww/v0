﻿using Earth.Ear.Ot.FantasyFootball.DataTransferObjects.PremierLeague;

namespace Earth.Ear.Ot.FantasyFootball.Models
{
    public class TeamViewModel
    {
        public FantasyFootballDto FantasyFootballDto { get; set; }

        public int SelectedTeamId { get; set; }
    }
}
